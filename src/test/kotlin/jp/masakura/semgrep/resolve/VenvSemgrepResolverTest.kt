package jp.masakura.semgrep.resolve

import jp.masakura.file.File
import jp.masakura.file.FileSystem
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull

class VenvSemgrepResolverTest {
    private lateinit var files: FakeFileSystem
    private lateinit var target: VenvSemgrepResolver

    @Before
    fun setUp() {
        files = FakeFileSystem()
        target = VenvSemgrepResolver("/home/user/projects/project1", files)
    }

    @Test
    fun testResolve() {
        files.add("/home/user/projects/project1/venv/bin/semgrep", File(true))

        val actual = target.resolve()

        assertEquals("/home/user/projects/project1/venv/bin/semgrep", actual)
    }

    @Test
    fun testResolveNotExists() {
        val actual = target.resolve()

        assertNull(actual)
    }

    @Test
    fun testResolveExistsButNotExecutable() {
        files.add("/home/user/projects/project1/venv/bin/semgrep", File(false))

        val actual = target.resolve()

        assertNull(actual)
    }
}

class FakeFileSystem : FileSystem {
    private val map = HashMap<String, File>()

    override fun get(path: String): File {
        return map.get(path) ?: File.notExists
    }

    fun add(path: String, file: File) {
        map.put(path, file)
    }
}
