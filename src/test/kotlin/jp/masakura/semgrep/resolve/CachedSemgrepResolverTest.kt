package jp.masakura.semgrep.resolve

import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull

class CachedSemgrepResolverTest {
    private lateinit var target: CachedSemgrepResolver

    @Before
    fun setUp() {
        target = CachedSemgrepResolver(OnceResolver("/home/user/.local/bin/semgrep"))
    }

    @Test
    fun testResolve() {
        val actual = target.resolve()

        assertEquals("/home/user/.local/bin/semgrep", actual)
    }

    @Test
    fun testResolveCached() {
        target.resolve()

        val actual = target.resolve()

        assertEquals("/home/user/.local/bin/semgrep", actual)
    }

    @Test
    fun testResolveCachedNull() {
        val target = CachedSemgrepResolver(OnceResolver(null))
        target.resolve()

        val actual = target.resolve()

        assertNull(actual)
    }
}

class OnceResolver(private val path: String?) : SemgrepResolver {
    private var times = 0

    override fun resolve(): String? {
        times++
        if (times > 1) throw IllegalStateException()
        return path
    }
}
