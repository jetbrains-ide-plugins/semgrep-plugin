package jp.masakura.semgrep.jetbrains.plugin

import jp.masakura.semgrep.configuration.SemgrepConfigurations
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue

class SemgrepSettingsTest {
    @Test
    fun testModifiedConfigurations() {
        val auto = SemgrepSettingsData("auto", "/bin/semgrep")
        val java = SemgrepSettingsData("r/java", "/bin/semgrep")

        assertTrue(auto.modified(java))
    }

    @Test
    fun testModifiedConfigurationsAppend() {
        val original = SemgrepSettingsData("r/java", "/bin/semgrep")
        val appended = SemgrepSettingsData("r/java,r/csharp", "/bin/semgrep")

        assertTrue(original.modified(appended))
    }

    @Test
    fun testModifiedExecutablePath() {
        val left = SemgrepSettingsData("auto", "/bin/semgrep")
        val right = SemgrepSettingsData("auto", "/usr/bin/semgrep")

        assertTrue(left.modified(right))
    }

    @Test
    fun testNotModified() {
        val left = SemgrepSettingsData("auto", "/bin/semgrep")
        val right = SemgrepSettingsData("auto", "/bin/semgrep")

        assertFalse(left.modified(right))
    }

    @Test
    fun testNotModifiedEmpties() {
        assertFalse(SemgrepSettingsData("", null).modified(SemgrepSettingsData("", null)))
    }

    class SemgrepSettingsData(configurations: String, override var executablePath: String?) : SemgrepSettings {
        override var configurations: SemgrepConfigurations = SemgrepConfigurations.parse(configurations)
    }
}
