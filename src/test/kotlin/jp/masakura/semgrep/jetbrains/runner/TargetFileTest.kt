package jp.masakura.semgrep.jetbrains.runner

import com.intellij.lang.FileASTNode
import com.intellij.lang.Language
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Key
import com.intellij.openapi.util.TextRange
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.vfs.VirtualFileSystem
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PsiDirectory
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiElementVisitor
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileSystemItem
import com.intellij.psi.PsiManager
import com.intellij.psi.PsiReference
import com.intellij.psi.ResolveState
import com.intellij.psi.scope.PsiScopeProcessor
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.search.PsiElementProcessor
import com.intellij.psi.search.SearchScope
import org.apache.commons.io.FilenameUtils
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.InputStream
import java.io.OutputStream
import javax.swing.Icon

class TargetFileTest {
    @Test
    fun testFrom() {
        val file = MockFile("content", MockVirtualFile("foo/Bar.java"))
        val actual = TargetFile.from(file).path

        assertEquals("foo/Bar.java", actual)
    }

    @Suppress("DeprecatedCallableAddReplaceWith")
    private class MockFile(
        private val _text: String,
        private val _virtualFile: MockVirtualFile,
    ) : PsiFile {

        override fun <T : Any?> getUserData(key: Key<T>): T? {
            throw Exception()
        }

        override fun <T : Any?> putUserData(key: Key<T>, value: T?) {
            throw Exception()
        }

        override fun getIcon(flags: Int): Icon {
            throw Exception()
        }

        override fun getProject(): Project {
            throw Exception()
        }

        override fun getLanguage(): Language {
            throw Exception()
        }

        override fun getManager(): PsiManager {
            throw Exception()
        }

        override fun getChildren(): Array<PsiElement> {
            throw Exception()
        }

        override fun getParent(): PsiDirectory? {
            throw Exception()
        }

        override fun getFirstChild(): PsiElement {
            throw Exception()
        }

        override fun getLastChild(): PsiElement {
            throw Exception()
        }

        override fun getNextSibling(): PsiElement {
            throw Exception()
        }

        override fun getPrevSibling(): PsiElement {
            throw Exception()
        }

        override fun getContainingFile(): PsiFile {
            throw Exception()
        }

        override fun getTextRange(): TextRange {
            throw Exception()
        }

        override fun getStartOffsetInParent(): Int {
            throw Exception()
        }

        override fun getTextLength(): Int {
            throw Exception()
        }

        override fun findElementAt(offset: Int): PsiElement? {
            throw Exception()
        }

        override fun findReferenceAt(offset: Int): PsiReference? {
            throw Exception()
        }

        override fun getTextOffset(): Int {
            throw Exception()
        }

        override fun getText(): String {
            return _text
        }

        override fun textToCharArray(): CharArray {
            throw Exception()
        }

        override fun getNavigationElement(): PsiElement {
            throw Exception()
        }

        override fun getOriginalElement(): PsiElement {
            throw Exception()
        }

        override fun textMatches(text: CharSequence): Boolean {
            throw Exception()
        }

        override fun textMatches(element: PsiElement): Boolean {
            throw Exception()
        }

        override fun textContains(c: Char): Boolean {
            throw Exception()
        }

        override fun accept(visitor: PsiElementVisitor) {
            throw Exception()
        }

        override fun acceptChildren(visitor: PsiElementVisitor) {
            throw Exception()
        }

        override fun copy(): PsiElement {
            throw Exception()
        }

        override fun add(element: PsiElement): PsiElement {
            throw Exception()
        }

        override fun addBefore(element: PsiElement, anchor: PsiElement?): PsiElement {
            throw Exception()
        }

        override fun addAfter(element: PsiElement, anchor: PsiElement?): PsiElement {
            throw Exception()
        }

        @Deprecated("Deprecated in Java")
        override fun checkAdd(element: PsiElement) {
            throw Exception()
        }

        override fun addRange(first: PsiElement?, last: PsiElement?): PsiElement {
            throw Exception()
        }

        override fun addRangeBefore(first: PsiElement, last: PsiElement, anchor: PsiElement?): PsiElement {
            throw Exception()
        }

        override fun addRangeAfter(first: PsiElement?, last: PsiElement?, anchor: PsiElement?): PsiElement {
            throw Exception()
        }

        override fun delete() {
            throw Exception()
        }

        @Deprecated("Deprecated in Java")
        override fun checkDelete() {
            throw Exception()
        }

        override fun deleteChildRange(first: PsiElement?, last: PsiElement?) {
            throw Exception()
        }

        override fun replace(newElement: PsiElement): PsiElement {
            throw Exception()
        }

        override fun isValid(): Boolean {
            throw Exception()
        }

        override fun isWritable(): Boolean {
            throw Exception()
        }

        override fun getReference(): PsiReference? {
            throw Exception()
        }

        override fun getReferences(): Array<PsiReference> {
            throw Exception()
        }

        override fun <T : Any?> getCopyableUserData(key: Key<T>): T? {
            throw Exception()
        }

        override fun <T : Any?> putCopyableUserData(key: Key<T>, value: T?) {
            throw Exception()
        }

        override fun processDeclarations(
            processor: PsiScopeProcessor,
            state: ResolveState,
            lastParent: PsiElement?,
            place: PsiElement,
        ): Boolean {
            throw Exception()
        }

        override fun getContext(): PsiElement? {
            throw Exception()
        }

        override fun isPhysical(): Boolean {
            throw Exception()
        }

        override fun getResolveScope(): GlobalSearchScope {
            throw Exception()
        }

        override fun getUseScope(): SearchScope {
            throw Exception()
        }

        override fun getNode(): FileASTNode {
            throw Exception()
        }

        override fun isEquivalentTo(another: PsiElement?): Boolean {
            throw Exception()
        }

        override fun getName(): String {
            throw Exception()
        }

        override fun setName(name: String): PsiElement {
            throw Exception()
        }

        override fun checkSetName(name: String?) {
            throw Exception()
        }

        override fun navigate(requestFocus: Boolean) {
            throw Exception()
        }

        override fun canNavigate(): Boolean {
            throw Exception()
        }

        override fun canNavigateToSource(): Boolean {
            throw Exception()
        }

        override fun getPresentation(): ItemPresentation? {
            throw Exception()
        }

        override fun isDirectory(): Boolean {
            throw Exception()
        }

        override fun getVirtualFile(): VirtualFile {
            return _virtualFile
        }

        override fun processChildren(processor: PsiElementProcessor<in PsiFileSystemItem>): Boolean {
            throw Exception()
        }

        override fun getContainingDirectory(): PsiDirectory {
            throw Exception()
        }

        override fun getModificationStamp(): Long {
            throw Exception()
        }

        override fun getOriginalFile(): PsiFile {
            throw Exception()
        }

        override fun getFileType(): FileType {
            throw Exception()
        }

        @Deprecated("Deprecated in Java")
        override fun getPsiRoots(): Array<PsiFile> {
            throw Exception()
        }

        override fun getViewProvider(): FileViewProvider {
            throw Exception()
        }

        override fun subtreeChanged() {
            throw Exception()
        }
    }

    private class MockVirtualFile(private val _path: String) : VirtualFile() {
        override fun toString(): String {
            return _path
        }

        override fun getName(): String {
            return FilenameUtils.getName(_path)
        }

        override fun getFileSystem(): VirtualFileSystem {
            throw Exception()
        }

        override fun getPath(): String {
            return _path
        }

        override fun isWritable(): Boolean {
            throw Exception()
        }

        override fun isDirectory(): Boolean {
            throw Exception()
        }

        override fun isValid(): Boolean {
            throw Exception()
        }

        override fun getParent(): VirtualFile {
            throw Exception()
        }

        override fun getChildren(): Array<VirtualFile> {
            throw Exception()
        }

        override fun getOutputStream(requestor: Any?, newModificationStamp: Long, newTimeStamp: Long): OutputStream {
            throw Exception()
        }

        override fun contentsToByteArray(): ByteArray {
            throw Exception()
        }

        override fun getTimeStamp(): Long {
            throw Exception()
        }

        override fun getLength(): Long {
            throw Exception()
        }

        override fun refresh(asynchronous: Boolean, recursive: Boolean, postRunnable: Runnable?) {
            throw Exception()
        }

        override fun getInputStream(): InputStream {
            throw Exception()
        }
    }
}
