package jp.masakura

import com.intellij.execution.configurations.GeneralCommandLine
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals

class GeneralCommandLineTest {
    @Test
    fun test() {
        val target = GeneralCommandLine()
            .withExePath("ls")
            .withParameters("-la")
            .withParameters("file.txt")

        assertEquals("ls -la file.txt", target.commandLineString)
    }
}
