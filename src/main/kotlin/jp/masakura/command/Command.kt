package jp.masakura.command

interface Command {
    fun run(parameters: List<String>): CommandResult

    companion object {
        fun factory(): CommandFactory {
            return CommandFactory()
        }
    }
}
