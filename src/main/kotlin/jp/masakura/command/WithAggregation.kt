package jp.masakura.command

import com.intellij.execution.configurations.GeneralCommandLine

class WithAggregation(private val aggregation: List<(GeneralCommandLine) -> GeneralCommandLine> = emptyList()) {
    fun add(with: (GeneralCommandLine) -> GeneralCommandLine): WithAggregation {
        return WithAggregation(aggregation + listOf(with))
    }

    fun commandLine(): GeneralCommandLine {
        var commandLine = GeneralCommandLine()
        for (with in aggregation) commandLine = with(commandLine)
        return commandLine
    }
}
