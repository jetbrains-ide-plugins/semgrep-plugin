package jp.masakura.command

class CommandResult(val input: StandardInput) {

    companion object {
        fun from(process: Process): CommandResult {
            return CommandResult(StandardInput(process.inputStream))
        }
    }
}
