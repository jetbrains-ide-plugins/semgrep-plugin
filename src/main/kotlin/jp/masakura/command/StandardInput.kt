package jp.masakura.command

import java.io.InputStream
import java.io.InputStreamReader
import java.util.Optional

class StandardInput(val stream: InputStream) {
    fun firstLine(): Optional<String> {
        return InputStreamReader(stream).readLines().stream().findFirst()
    }
}
