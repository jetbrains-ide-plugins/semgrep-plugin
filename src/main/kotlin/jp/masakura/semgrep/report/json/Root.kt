package jp.masakura.semgrep.report.json

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Root(val version: String?, val results: List<Result>) {
    companion object {
        val empty = Root(null, emptyList())
    }
}
