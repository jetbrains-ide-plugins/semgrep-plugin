package jp.masakura.semgrep.scanning

import jp.masakura.command.Command
import jp.masakura.semgrep.configuration.SemgrepConfigurations
import jp.masakura.semgrep.report.SemgrepReport

class SemgrepScanner(
    executablePath: String,
    workingDirectory: String,
    configurations: SemgrepConfigurations
) {
    private val semgrep = Command.factory()
        .with { it.withExePath(executablePath) }
        .with { it.withWorkDirectory(workingDirectory) }
        .with { it.withParameters(listOf("--json", "--disable-version-check")) }
        .with { it.withParameters(configurations.toParameters2()) }
        .command()

    fun scan(path: String): SemgrepReport {
        val result = semgrep.run(listOf(path))
        return SemgrepReport.from(result.input.stream)
    }
}

private fun SemgrepConfigurations.toParameters2(): List<String> {
    return values().map { "--config=$it" }
}
