package jp.masakura.semgrep.jetbrains.file

import java.io.Closeable

interface TemporaryFile : Closeable {
    val path: String
    fun save(content: String): TemporaryFile
    fun delete()
    override fun close() {
        delete()
    }
}
