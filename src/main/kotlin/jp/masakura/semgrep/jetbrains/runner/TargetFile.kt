package jp.masakura.semgrep.jetbrains.runner

import com.intellij.psi.PsiFile

data class TargetFile(
    val path: String,
    val extension: String?,
    val content: String,
) {
    override fun toString(): String {
        return path
    }

    companion object {
        fun from(file: PsiFile): TargetFile {
            return TargetFile(
                file.virtualFile.path,
                file.virtualFile.extension,
                file.text,
            )
        }
    }
}
