package jp.masakura.semgrep.jetbrains.runner

import jp.masakura.semgrep.jetbrains.file.TemporaryFile
import jp.masakura.semgrep.jetbrains.file.TemporaryFileFactory
import java.io.Closeable

class TemporaryTargetFile(private val temporaryFile: TemporaryFile, val file: TargetFile) : Closeable {
    override fun close() {
        temporaryFile.close()
    }

    companion object {
        fun create(temporaryFileFactory: TemporaryFileFactory, file: TargetFile): TemporaryTargetFile {
            val temporaryFile = temporaryFileFactory
                .create(getExtension(file.extension))
                .save(file.content)
            return TemporaryTargetFile(temporaryFile, TargetFile(temporaryFile.path, file.extension, file.content))
        }
    }
}

fun TargetFile.createTemporaryFile(temporaryFileFactory: TemporaryFileFactory): TemporaryTargetFile {
    return TemporaryTargetFile.create(temporaryFileFactory, this)
}

private fun getExtension(extension: String?): String? {
    if (extension == null) return null
    if (extension.trim() == "") return null
    return ".$extension"
}
