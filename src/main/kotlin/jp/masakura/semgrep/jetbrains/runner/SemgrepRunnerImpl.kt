package jp.masakura.semgrep.jetbrains.runner

import jp.masakura.semgrep.report.SemgrepReport
import jp.masakura.semgrep.scanning.SemgrepScanner

class SemgrepRunnerImpl(private val scanner: SemgrepScanner) : SemgrepRunner {
    override fun run(file: TargetFile): SemgrepReport {
        return scanner.scan(file.path)
    }
}
