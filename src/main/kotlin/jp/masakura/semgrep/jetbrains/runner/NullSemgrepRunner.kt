package jp.masakura.semgrep.jetbrains.runner

import jp.masakura.semgrep.report.SemgrepReport

class NullSemgrepRunner : SemgrepRunner {
    override fun run(file: TargetFile): SemgrepReport {
        return SemgrepReport.empty
    }

    companion object {
        val default: SemgrepRunner = NullSemgrepRunner()
    }
}
