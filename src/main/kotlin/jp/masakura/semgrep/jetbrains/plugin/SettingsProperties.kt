package jp.masakura.semgrep.jetbrains.plugin

import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.project.Project

class SettingsProperties(private val properties: PropertiesComponent) {
    fun property(name: String): PropertyValue {
        return PropertyValue(name, properties)
    }

    companion object {
        fun project(project: Project): SettingsProperties {
            return SettingsProperties(PropertiesComponent.getInstance(project))
        }
    }
}
