package jp.masakura.semgrep.jetbrains.plugin

import com.intellij.psi.PsiFile
import jp.masakura.semgrep.jetbrains.runner.SemgrepRunner
import jp.masakura.semgrep.jetbrains.runner.TargetFile
import jp.masakura.semgrep.report.SemgrepReport

class TargetInspection(
    private val target: TargetFile,
    private val runner: SemgrepRunner,
) {
    fun inspect(): SemgrepReport {
        return runner.run(target)
    }

    companion object {
        fun from(file: PsiFile?): TargetInspection? {
            if (file == null) return null

            return TargetInspection(TargetFile.from(file), SemgrepRunner.project(file.project))
        }
    }
}
