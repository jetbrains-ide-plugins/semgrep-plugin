package jp.masakura.semgrep.jetbrains.plugin

import jp.masakura.semgrep.configuration.SemgrepConfigurations

interface SemgrepSettings {
    var configurations: SemgrepConfigurations
    val executablePath: String?
}

fun SemgrepSettings.modified(other: SemgrepSettings): Boolean {
    return configurations != other.configurations || executablePath != other.executablePath
}
