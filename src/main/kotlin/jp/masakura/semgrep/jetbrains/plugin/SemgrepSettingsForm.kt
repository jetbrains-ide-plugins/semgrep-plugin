package jp.masakura.semgrep.jetbrains.plugin

import jp.masakura.semgrep.configuration.SemgrepConfigurations
import jp.masakura.semgrep.resolve.SemgrepResolver
import javax.swing.JPanel
import javax.swing.JTextField

class SemgrepSettingsForm(private val semgrepResolver: SemgrepResolver) : SemgrepSettings {
    lateinit var root: JPanel
    private lateinit var configurationsText: JTextField
    private lateinit var executablePathText: JTextField

    override var configurations: SemgrepConfigurations
        get() = SemgrepConfigurations.parse(configurationsText.text)
        set(value) {
            configurationsText.text = value.toString()
        }

    override var executablePath: String?
        get() = semgrepResolver.resolve()
        set(value) {
            executablePathText.text = value ?: "Not found"
        }
}
