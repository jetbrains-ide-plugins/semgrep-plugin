package jp.masakura.semgrep.jetbrains.plugin

import com.intellij.openapi.options.Configurable
import com.intellij.openapi.project.Project
import jp.masakura.semgrep.resolve.SemgrepResolver
import javax.swing.JComponent

class SemgrepSettingsConfigurable(project: Project) : Configurable {
    private var component: SemgrepSettingsForm? = null
    private val properties = PropertiesSemgrepSettings.project(project)
    private val semgrepResolver = SemgrepResolver.project(project)

    override fun createComponent(): JComponent {
        component = SemgrepSettingsForm(semgrepResolver)
        return component!!.root
    }

    override fun reset() {
        val c = component!!
        c.configurations = properties.configurations
        c.executablePath = properties.executablePath
    }

    override fun disposeUIResources() {
        component = null
    }

    override fun isModified(): Boolean {
        return component!!.modified(properties)
    }

    override fun apply() {
        properties.configurations = component!!.configurations
    }

    override fun getDisplayName(): String {
        return "Semgrep"
    }
}
