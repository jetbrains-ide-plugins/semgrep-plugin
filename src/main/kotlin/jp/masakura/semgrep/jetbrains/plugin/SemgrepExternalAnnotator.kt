package jp.masakura.semgrep.jetbrains.plugin

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.ExternalAnnotator
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.editor.Editor
import com.intellij.psi.PsiFile
import jp.masakura.semgrep.jetbrains.annotation.SemgrepAnnotation
import jp.masakura.semgrep.report.SemgrepReport

class SemgrepExternalAnnotator : ExternalAnnotator<TargetInspection, SemgrepReport>() {
    override fun collectInformation(file: PsiFile): TargetInspection? {
        return TargetInspection.from(file)
    }

    override fun collectInformation(file: PsiFile, editor: Editor, hasErrors: Boolean): TargetInspection? {
        return collectInformation(file)
    }

    override fun doAnnotate(collectedInfo: TargetInspection?): SemgrepReport? {
        logger.debug("doAnnotate()")

        if (collectedInfo == null) return null

        return collectedInfo.inspect()
    }

    override fun apply(file: PsiFile, annotationResult: SemgrepReport?, holder: AnnotationHolder) {
        logger.debug("doApply()")

        if (annotationResult == null) return

        SemgrepAnnotation.from(annotationResult)
            .forEach {
                holder.newAnnotation(it.severity, it.message)
                    .range(it.range)
                    .create()
            }
    }

    companion object {
        private val logger = Logger.getInstance(SemgrepExternalAnnotator::class.java)
    }
}
