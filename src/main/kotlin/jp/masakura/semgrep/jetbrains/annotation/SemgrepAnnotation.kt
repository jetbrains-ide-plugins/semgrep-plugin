package jp.masakura.semgrep.jetbrains.annotation

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.TextRange
import jp.masakura.semgrep.report.SemgrepReport
import jp.masakura.semgrep.report.json.Result
import jp.masakura.semgrep.report.json.Severity

data class SemgrepAnnotation(
    val severity: HighlightSeverity,
    val range: TextRange,
    val message: String,
) {
    companion object {
        fun from(report: SemgrepReport): List<SemgrepAnnotation> {
            return report.root.results.map { from(it) }
        }

        fun from(result: Result): SemgrepAnnotation = SemgrepAnnotation(
            highlightSeverity(result.extra.severity),
            textRange(result),
            "Semgrep: ${result.extra.message}",
        )

        private fun highlightSeverity(severity: Severity): HighlightSeverity = when (severity) {
            Severity.Error -> HighlightSeverity.ERROR
            Severity.Warning -> HighlightSeverity.WARNING
            Severity.Info -> HighlightSeverity.INFORMATION
        }

        private fun textRange(result: Result) = TextRange(
            result.start.offset,
            result.end.offset,
        )
    }
}
