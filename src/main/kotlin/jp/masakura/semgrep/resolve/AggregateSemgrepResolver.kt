package jp.masakura.semgrep.resolve

class AggregateSemgrepResolver(private val resolvers: List<SemgrepResolver>) : SemgrepResolver {
    override fun resolve(): String? {
        for (resolver in resolvers) {
            val path = resolver.resolve()
            if (path != null) return path
        }
        return null
    }
}
