package jp.masakura.semgrep.resolve

class CachedSemgrepResolver(private val resolver: SemgrepResolver) : SemgrepResolver {
    private var cached = false
    private var value: String? = null

    override fun resolve(): String? {
        if (!cached) {
            cached = true
            value = resolver.resolve()
        }
        return value
    }
}
