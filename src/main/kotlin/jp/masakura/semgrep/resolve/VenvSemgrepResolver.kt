package jp.masakura.semgrep.resolve

import jp.masakura.file.FileSystem
import java.nio.file.Paths

class VenvSemgrepResolver(
    private val projectDirectory: String,
    private val files: FileSystem = FileSystem.default,
) : SemgrepResolver {
    override fun resolve(): String? {
        val path = Paths.get(projectDirectory).resolve("venv/bin/semgrep").toString()

        if (files.get(path).canExecute) return path
        return null
    }
}
