package jp.masakura.semgrep.resolve

import com.intellij.openapi.project.Project
import jp.masakura.utility.ObjectProvider

interface SemgrepResolver {
    fun resolve(): String?

    companion object {
        private val resolvers = ObjectProvider<Project, SemgrepResolver> { create(it) }
        private val logger = com.intellij.openapi.diagnostic.Logger.getInstance(SemgrepResolver::class.java)

        fun project(project: Project): SemgrepResolver {
            return resolvers.get(project)
        }

        private fun create(project: Project): CachedSemgrepResolver {
            logger.debug("createSemgrepResolver($project)")

            return CachedSemgrepResolver(
                AggregateSemgrepResolver(
                    listOf(
                        VenvSemgrepResolver(project.basePath!!),
                        WhichSemgrepResolver.default,
                    ),
                ),
            )
        }
    }
}
