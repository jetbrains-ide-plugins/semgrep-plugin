package jp.masakura.semgrep.resolve

import jp.masakura.command.Command

class WhichSemgrepResolver(private val command: Command) : SemgrepResolver {
    override fun resolve(): String? {
        val line = command.run(listOf("semgrep")).input.firstLine()
        return line.orElse(null)
    }

    companion object {
        private val which = Command.factory().with { it.withExePath("which") }.command()
        val default = WhichSemgrepResolver(which)
    }
}
