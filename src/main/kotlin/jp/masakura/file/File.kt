package jp.masakura.file

class File(val canExecute: Boolean) {
    companion object {
        fun from(file: java.io.File): File {
            if (!file.exists()) return notExists
            return File(file.canExecute())
        }

        val notExists = File(false)
    }
}
