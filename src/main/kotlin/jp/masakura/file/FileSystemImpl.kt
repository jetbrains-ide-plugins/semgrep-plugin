package jp.masakura.file

class FileSystemImpl : FileSystem {
    override fun get(path: String): File {
        val file = java.io.File(path)
        return File.from(file)
    }
}
