# Unofficial Semgrep plugin for JetBrains IDE
The Semgrep plugin displays issues of Semgrep, an open source static analysis tool, in the editor.

![](./docs/assets/screenshot.png)

Supports IntelliJ IDE, WebStorm, Rider and other JetBrains IDEs.

See [Supported languages](https://semgrep.dev/docs/supported-languages/) for the languages supported by semgrep.


## How to use
1. Install Semgrep as in `pip install semgrep`.
2. Install [Semgrep plugin](https://plugins.jetbrains.com/plugin/20802-semgrep) in your JetBrains IDE.
3. Open the IDE settings, select Semgrep and set Configuration to `auto` or `r/java` or `semgrep.yml`.
4. Open your sourc code.


## Configuration
* `Configuration` - Value for `--config` of the sempgre command. Multiple values may be specified, separated by commas. Default is `auto`.

`auto` takes a long time to inspect. Please set other than `auto`.
If possible, download the semgrep config file, include it in your project, and configure it.


## LICENSE
MIT License
